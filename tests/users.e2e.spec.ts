import { App } from '../src/app';
import { boot } from '../src/main';
import request from 'supertest';

let application: App;

beforeAll(async () => {
	const reaApp = await boot;
	application = reaApp.app;
});

describe('user service', async () => {
	await application.init();

	it('register - err', async () => {
		const res = await request(application.app)
			.post('/users/register')
			.send({ email: 'test@mail.ru', password: '1' });
		expect(res.statusCode).toBe(422);
	});

	it('register - success', async () => {
		const res = await request(application.app)
			.post('/users/register')
			.send({ email: 'test12@mail.ru', password: '1', name: 'Daniel' });
		expect(res.statusCode).toBe(422);
	});
});

afterAll(() => {
	application.close();
});
