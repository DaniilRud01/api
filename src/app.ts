import 'reflect-metadata';
import express, { Express } from 'express';
import { Server } from 'http';
import { inject, injectable } from 'inversify';
import { AuthMiddleware } from './common/auth.middleware.js';
import { IConfig } from './config/config.interface.js';
import { PrismaService } from './database/prisma.service.js';
import { ExceptionFilter } from './errors/exception-filter.js';
import { ILoggerService } from './logger/logger.service.interface.js';
import { TYPE } from './types.js';
import { UserController } from './users/user.controller.js';
import bodyParser from 'body-parser';

@injectable()
export class App {
	port: number;
	app: Express;
	server: Server;

	constructor(
		@inject(TYPE.ILoggerService) private logger: ILoggerService,
		@inject(TYPE.UserController) private userController: UserController,
		@inject(TYPE.IExceptionFilterInterface) private exceptionFilter: ExceptionFilter,
		@inject(TYPE.PrismaService) private prismaService: PrismaService,
		@inject(TYPE.ConfigService) private config: IConfig,
	) {
		this.app = express();
		this.port = 8000;
		this.logger = logger;
		this.userController = userController;
		this.exceptionFilter = exceptionFilter;
	}

	public useMiddleware(): void {
		this.app.use(bodyParser.json());
		const authMiddleware = new AuthMiddleware(this.config.get('SECRET_KEY'));
		this.app.use(authMiddleware.execute.bind(authMiddleware));
	}

	public useRoutes(): void {
		this.app.use('/users', this.userController.router);
	}

	public useExceptionFilter(): void {
		this.app.use(this.exceptionFilter.catch.bind(this.exceptionFilter));
	}

	public async init(): Promise<void> {
		this.useMiddleware();
		this.useRoutes();
		this.useExceptionFilter();
		await this.prismaService.connect();
		this.app.listen(this.port, '127.0.0.1');
		this.logger.log(`Server started port: ${this.port}`);
	}

	public close(): void {
		this.server.close();
	}
}
