import { Container } from 'inversify';
import { App } from './app.js';
import { ConfigService } from './config/config-service.js';

export const TYPE = {
	Application: Symbol.for('Application'),
	ILoggerService: Symbol.for('ILoggerService'),
	IExceptionFilterInterface: Symbol.for('IExceptionFilterInterface'),
	UserController: Symbol.for('IUserController'),
	UserService: Symbol.for('UserService'),
	IUserRepository: Symbol.for('IUserRepository'),
	ConfigService: Symbol.for('ConfigService'),
	PrismaService: Symbol.for('PrismaService'),
};

export interface IBootstrap {
	app: App;
	appContainer: Container;
}
