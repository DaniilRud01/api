import { ClassConstructor, plainToClass } from 'class-transformer';
import { validate } from 'class-validator';
import { IMiddleware } from './middleware.interface.js';
import { NextFunction, Request, Response } from 'express';

export class ValidateMiddleware implements IMiddleware {
	constructor(private ValidateClass: ClassConstructor<object>) {}

	async execute({ body }: Request, res: Response, next: NextFunction): Promise<void> {
		const instance = plainToClass(this.ValidateClass, body);
		const error = await validate(instance);
		if (error.length > 0) {
			res.status(422).send(error);
		} else {
			next();
		}
	}
}
