import { NextFunction, Request, Response, Router } from 'express';
import { IMiddleware } from './middleware.interface.js';

interface IControllerRoute {
	path: string;
	method: keyof Pick<Router, 'get' | 'post' | 'put' | 'patch' | 'delete'>;
	func: (request: Request, response: Response, next: NextFunction) => void;
	middlewares?: IMiddleware[];
}

export type { IControllerRoute };
