import 'reflect-metadata';
import { Router } from 'express';
import { injectable } from 'inversify';
import { ILoggerService } from '../logger/logger.service.interface.js';
import { IMiddleware } from './middleware.interface.js';
import { IControllerRoute } from './route.interface.js';

@injectable()
export abstract class BaseController {
	private readonly _router: Router;

	constructor(private logger: ILoggerService) {
		this._router = Router();
	}

	get router(): Router {
		return this._router;
	}

	protected buildRouter(routers: IControllerRoute[]): void {
		for (const route of routers) {
			this.logger.log(`[${route.method}], [${route.path}]`);
			const middleware = route.middlewares?.map((m: IMiddleware) => m.execute.bind(m));
			const handler = middleware ? [...middleware, route.func.bind(this)] : route.func.bind(this);
			this.router[route.method](route.path, handler);
		}
	}
}
