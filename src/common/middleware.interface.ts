import { Request, Response, NextFunction } from 'express';

interface IMiddleware {
	execute: (request: Request, response: Response, next: NextFunction) => void;
}

export type { IMiddleware };
