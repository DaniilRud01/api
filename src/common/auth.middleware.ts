import { Request, Response, NextFunction } from 'express';
import { JwtPayload, verify } from 'jsonwebtoken';
import { IMiddleware } from './middleware.interface.js';

export class AuthMiddleware implements IMiddleware {
	constructor(private secretKey: string) {}

	execute(req: Request, res: Response, next: NextFunction): void {
		const token = req.headers.authorization?.split(' ')[1];
		if (token) {
			verify(token, this.secretKey, (err, payload) => {
				if (err) {
					next();
				} else if (payload) {
					const { email } = payload as JwtPayload;
					req.user = email;
					next();
				}
			});
		} else {
			next();
		}
	}
}
