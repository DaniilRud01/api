import 'reflect-metadata';
import { NextFunction, Request, Response } from 'express';
import { inject, injectable } from 'inversify';
import { LoggerService } from '../logger/loger.service.js';
import { ILoggerService } from '../logger/logger.service.interface.js';
import { TYPE } from '../types.js';
import { IExceptionFilterInterface } from './exception-filter.interface.js';
import { HttpError } from './http-error.js';

@injectable()
export class ExceptionFilter implements IExceptionFilterInterface {
	constructor(@inject(TYPE.ILoggerService) private logger: ILoggerService) {}

	catch(err: Error | HttpError, req: Request, res: Response, next: NextFunction) {
		if (err instanceof HttpError) {
			this.logger.error(`${err?.context ? err.context : ''} ${err.statusCode} ${err.message}`);
			res.status(err.statusCode).send({ error: err.message });
		} else {
			this.logger.error(`${err.message}`);
			res.status(500).send({ err: err.message });
		}
	}
}
