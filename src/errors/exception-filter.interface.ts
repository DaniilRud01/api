import { NextFunction, Request, Response } from 'express';

interface IExceptionFilterInterface {
	catch: (err: Error, req: Request, res: Response, next: NextFunction) => void;
}

export type { IExceptionFilterInterface };
