import { PrismaClient } from '@prisma/client';
import { inject, injectable } from 'inversify';
import { ILoggerService } from '../logger/logger.service.interface.js';
import { TYPE } from '../types.js';

@injectable()
export class PrismaService {
	public client: PrismaClient;

	constructor(@inject(TYPE.ILoggerService) private logger: ILoggerService) {
		this.client = new PrismaClient();
	}

	async connect(): Promise<void> {
		try {
			this.client.$connect();
			this.logger.log('db connected...');
		} catch (e) {
			if (e instanceof Error) {
				this.logger.error(`error connection db ${e.message}`);
			}
		}
	}

	async disconnect(): Promise<void> {
		this.client.$disconnect();
		this.logger.log('db disconnected...');
	}
}
