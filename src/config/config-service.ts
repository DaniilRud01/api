import { config, DotenvConfigOutput, DotenvParseOutput } from 'dotenv';
import { inject, injectable } from 'inversify';
import { ILoggerService } from '../logger/logger.service.interface.js';
import { TYPE } from '../types.js';
import { IConfig } from './config.interface.js';

@injectable()
export class ConfigService implements IConfig {
	private config: DotenvParseOutput;
	constructor(@inject(TYPE.ILoggerService) private logger: ILoggerService) {
		const readConfig: DotenvConfigOutput = config();
		if (readConfig.error) {
			this.logger.error('error read config');
		} else {
			this.config = readConfig.parsed as DotenvParseOutput;
			this.logger.log('read config success');
		}
	}

	get(key: string): string {
		return this.config[key];
	}
}
