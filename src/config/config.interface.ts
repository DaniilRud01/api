interface IConfig {
	get: (key: string) => string;
}

export type { IConfig };
