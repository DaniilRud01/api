import { Container, ContainerModule, interfaces } from 'inversify';
import { App } from './app.js';
import { IConfig } from './config/config.interface.js';
import { ConfigService } from './config/config-service.js';
import { PrismaService } from './database/prisma.service.js';
import { IExceptionFilterInterface } from './errors/exception-filter.interface.js';
import { ExceptionFilter } from './errors/exception-filter.js';
import { LoggerService } from './logger/loger.service.js';
import { ILoggerService } from './logger/logger.service.interface.js';
import { IBootstrap, TYPE } from './types.js';
import { IUserService } from './users/user-service.interface.js';
import { IUserController } from './users/user.controller.interface.js';
import { UserController } from './users/user.controller.js';
import { IUserRepository } from './users/user.repository.interface.js';
import { UserRepository } from './users/user.repository.js';
import { UserService } from './users/user.service.js';

const appBuildings = new ContainerModule((bind: interfaces.Bind) => {
	bind<App>(TYPE.Application).to(App);
	bind<ILoggerService>(TYPE.ILoggerService).to(LoggerService).inSingletonScope();
	bind<IExceptionFilterInterface>(TYPE.IExceptionFilterInterface).to(ExceptionFilter);
	bind<IUserController>(TYPE.UserController).to(UserController);
	bind<IUserRepository>(TYPE.IUserRepository).to(UserRepository);
	bind<IConfig>(TYPE.ConfigService).to(ConfigService).inSingletonScope();
	bind<PrismaService>(TYPE.PrismaService).to(PrismaService).inSingletonScope();
	bind<IUserService>(TYPE.UserService).to(UserService);
});

async function bootstrapApp(): Promise<IBootstrap> {
	const appContainer = new Container();
	appContainer.load(appBuildings);
	const app = appContainer.get<App>(TYPE.Application);
	await app.init();
	return { app, appContainer };
}

export const boot = bootstrapApp();
