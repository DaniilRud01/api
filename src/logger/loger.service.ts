import 'reflect-metadata';
import { injectable } from 'inversify';
import { Logger } from 'tslog';
import { ILogObj } from 'tslog/dist/types/interfaces';
import { ILoggerService } from './logger.service.interface.js';

@injectable()
export class LoggerService implements ILoggerService {
	public logger: Logger<ILogObj>;

	constructor() {
		this.logger = new Logger({
			hideLogPositionForProduction: false,
			type: 'pretty',
		});
	}

	log(...args: unknown[]): void {
		this.logger.info(args);
	}

	warn(...args: unknown[]): void {
		this.logger.warn(args);
	}

	error(...args: unknown[]): void {
		this.logger.error(args);
	}
}
