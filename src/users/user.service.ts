import { UserModel } from '@prisma/client';
import { inject, injectable } from 'inversify';
import { IConfig } from '../config/config.interface.js';
import { TYPE } from '../types.js';
import { UserLoginDto } from './dto/user-login.dto.js';
import { UserRegisterDto } from './dto/user-register.dto.js';
import { IUserService } from './user-service.interface.js';
import { UserEntity } from './user.entity.js';
import { IUserRepository } from './user.repository.interface.js';

@injectable()
export class UserService implements IUserService {
	constructor(
		@inject(TYPE.ConfigService) private config: IConfig,
		@inject(TYPE.IUserRepository) private userRepository: IUserRepository,
	) {}

	async createUser({ name, password, email }: UserRegisterDto): Promise<UserModel | null> {
		const existedUser = await this.userRepository.find(email);
		if (existedUser) {
			return null;
		} else {
			const newUser = new UserEntity(email, name);
			const salt = this.config.get('SALT');
			await newUser.setPassword(password, +salt);
			return this.userRepository.create(newUser);
		}
	}

	async validUser({ email, password }: UserLoginDto): Promise<boolean> {
		const existUser = await this.userRepository.find(email);
		if (existUser) {
			const newUser = new UserEntity(existUser.name, existUser.email, existUser.password);
			return newUser.comparePassword(password);
		}
		return false;
	}

	async getUserInfo(email: string): Promise<UserModel | null> {
		return await this.userRepository.find(email);
	}
}
