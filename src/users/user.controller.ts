import 'reflect-metadata';
import { NextFunction, Request, Response } from 'express';
import { inject, injectable } from 'inversify';
import jwt from 'jsonwebtoken';
import { BaseController } from '../common/base.controller.js';
import { GuardMiddleware } from '../common/guard.middleware.js';
import { ValidateMiddleware } from '../common/validate.middleware.js';
import { IConfig } from '../config/config.interface.js';
import { HttpError } from '../errors/http-error.js';
import { ILoggerService } from '../logger/logger.service.interface.js';
import { TYPE } from '../types.js';
import { UserLoginDto } from './dto/user-login.dto.js';
import { UserRegisterDto } from './dto/user-register.dto.js';
import { IUserService } from './user-service.interface.js';
import { IUserController } from './user.controller.interface.js';
import { UserEntity } from './user.entity.js';

@injectable()
export class UserController extends BaseController implements IUserController {
	constructor(
		@inject(TYPE.ILoggerService) public loggerService: ILoggerService,
		@inject(TYPE.UserService) public userService: IUserService,
		@inject(TYPE.ConfigService) public configService: IConfig,
	) {
		super(loggerService);
		this.buildRouter([
			{
				path: '/login',
				method: 'post',
				func: this.login,
				middlewares: [new ValidateMiddleware(UserLoginDto)],
			},
			{
				path: '/register',
				method: 'post',
				func: this.register,
				middlewares: [new ValidateMiddleware(UserRegisterDto)],
			},
			{
				path: '/info',
				method: 'get',
				func: this.info,
				middlewares: [new GuardMiddleware()],
			},
		]);
	}

	async login({ body }: Request, res: Response, next: NextFunction): Promise<void> {
		const successLogin = await this.userService.validUser(body);
		if (successLogin) {
			const jwt = await this.signJwt(body.email, this.configService.get('SECRET_KEY'));
			res.status(200).send({ jwt });
			this.loggerService.log(`logged user: ${body.email}`);
		} else {
			return next(new HttpError(401, 'Unauthorized', 'login'));
		}
	}

	async register(
		{ body }: Request<unknown, unknown, UserEntity>,
		res: Response,
		next: NextFunction,
	): Promise<void> {
		const result = await this.userService.createUser(body);
		if (!result) {
			next(new HttpError(422, 'Пользователь существует'));
		} else {
			res.status(200).send({ id: result.id, email: result.email });
		}
	}

	private signJwt(email: string, secret: string): Promise<string> {
		return new Promise((resolve, reject) => {
			jwt.sign(
				{
					email,
					iet: Math.floor(Date.now() / 1000),
				},
				secret,
				{
					algorithm: 'HS256',
				},
				(error, encoded) => {
					if (error) {
						reject(error);
					}
					resolve(encoded as string);
				},
			);
		});
	}

	async info({ user }: Request, res: Response, next: NextFunction): Promise<void> {
		const userInfo = await this.userService.getUserInfo(user);
		if (userInfo) {
			res.send({
				userInfo,
			});
		}
	}
}
