import { UserModel } from '@prisma/client';
import { inject, injectable } from 'inversify';
import { PrismaService } from '../database/prisma.service.js';
import { TYPE } from '../types.js';
import { UserEntity } from './user.entity.js';
import { IUserRepository } from './user.repository.interface.js';

@injectable()
export class UserRepository implements IUserRepository {
	constructor(@inject(TYPE.PrismaService) private prismaService: PrismaService) {}

	async create({ email, name, password }: UserEntity): Promise<UserModel> {
		return await this.prismaService.client.userModel.create({
			data: {
				email,
				name,
				password,
			},
		});
	}

	async find(email: string): Promise<UserModel | null> {
		return await this.prismaService.client.userModel.findFirst({
			where: { email },
		});
	}
}
