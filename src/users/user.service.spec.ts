import 'reflect-metadata';
import { UserModel } from '@prisma/client';
import { Container } from 'inversify';
import { IConfig } from '../config/config.interface';
import { TYPE } from './../types';
import { IUserService } from './user-service.interface';
import { UserEntity } from './user.entity';
import { IUserRepository } from './user.repository.interface';
import { UserService } from './user.service';

const container = new Container();
let configService: IConfig;
let userService: IUserService;
let userRepository: IUserRepository;

const ConfigServiceMock: IConfig = {
	get: jest.fn(),
};

const UserRepositoryMock: IUserRepository = {
	create: jest.fn(),
	find: jest.fn(),
};

beforeAll(() => {
	container.bind<IUserService>(TYPE.UserService).to(UserService);
	container.bind<IUserRepository>(TYPE.IUserRepository).toConstantValue(UserRepositoryMock);
	container.bind<IConfig>(TYPE.ConfigService).toConstantValue(ConfigServiceMock);

	configService = container.get<IConfig>(TYPE.ConfigService);
	userService = container.get<IUserService>(TYPE.UserService);
	userRepository = container.get<IUserRepository>(TYPE.IUserRepository);
});

describe('User service', () => {
	it('create user', async () => {
		ConfigServiceMock.get = jest.fn().mockReturnValueOnce('1');
		userRepository.create = jest.fn().mockImplementationOnce((user: UserEntity): UserModel => {
			return {
				email: user.email,
				name: user.name,
				password: user.password,
				id: 1,
			};
		});
		const createdUser = await userService.createUser({
			name: 'Daniel',
			password: '1',
			email: 'test@mail.ru',
		});
		await UserRepositoryMock.find('test@mail.ru');
		expect(createdUser?.id).toEqual(1);
		expect(createdUser?.password).not.toEqual('1');
	});

	it('validate user - success', async () => {
		userRepository.find = jest.fn().mockReturnValueOnce('test@mail.ru');
		const res = await userService.validUser({
			email: 'test@mail.ru',
			password: '1',
		});
		expect(res).toBeTruthy();
	});

	it('validate user - wrong password', async () => {
		userRepository.find = jest.fn().mockReturnValueOnce('test@mail.ru');
		const res = await userService.validUser({ email: 'test@mail.ru', password: '2' });
		expect(res).toBeFalsy();
	});

	it('validate user - wrong user', async () => {
		userRepository.find = jest.fn().mockReturnValueOnce('test@mail.ru');
		const res = await userService.validUser({ email: 'test2@mail.ru', password: '2' });
		expect(res).toBeFalsy();
	});
});
