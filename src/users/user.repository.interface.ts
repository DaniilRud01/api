import { UserModel } from '@prisma/client';
import { UserEntity } from './user.entity.js';

interface IUserRepository {
	create: (user: UserEntity) => Promise<UserModel>;
	find: (email: string) => Promise<UserModel | null>;
}

export type { IUserRepository };
