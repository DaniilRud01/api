import { NextFunction, Request, Response } from 'express';

interface IUserController {
	login: (req: Request, res: Response, next: NextFunction) => void;
	register: (req: Request, res: Response, next: NextFunction) => void;
}

export type { IUserController };
