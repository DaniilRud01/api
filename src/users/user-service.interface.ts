import { UserModel } from '@prisma/client';
import { UserLoginDto } from './dto/user-login.dto.js';
import { UserRegisterDto } from './dto/user-register.dto.js';

interface IUserService {
	createUser: (dto: UserRegisterDto) => Promise<UserModel | null>;
	validUser: (user: UserLoginDto) => Promise<boolean>;
	getUserInfo: (email: string) => Promise<UserModel | null>;
}

export type { IUserService };
